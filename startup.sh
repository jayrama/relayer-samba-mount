#!/bin/bash

# Create the rclone config file dynamically
mkdir -p /root/.config/rclone
cat <<EOF > /root/.config/rclone/rclone.conf
[relayer]
type = s3
provider = Other
env_auth = false
access_key_id = $MINIO_ACCESS_KEY
secret_access_key = $MINIO_SECRET_KEY
endpoint = 127.0.0.1:9000
EOF

# Create a new Samba config file dynamically (only global section)
cat <<EOF > /etc/samba/smb.conf
[global]
   workgroup = WORKGROUP
   server string = Samba Server
   netbios name = samba
   security = user
   map to guest = Bad User
   guest account = nobody
   load printers = no
   printcap name = /dev/null
   disable spoolss = yes
   log file = /var/log/samba/%m.log
   max log size = 50
   dns proxy = no
EOF

# Function to add an S3 bucket as a Samba share
add_samba_share() {
    local bucket_name="$1"
    local mount_path="/mnt/$bucket_name"

    # Add bucket to Samba config if not already present
    if ! grep -q "\[$bucket_name\]" /etc/samba/smb.conf; then
        echo "Adding Samba share for $bucket_name..."
        cat <<EOF >> /etc/samba/smb.conf

[$bucket_name]
   path = $mount_path
   browseable = yes
   writable = yes
   guest ok = no
   valid users = $SHARE_USERNAME
   create mask = 0755
   directory mask = 0755
   force create mode = 0755
   force directory mode = 0755
EOF
    fi
}

# Function to remove a Samba share for a bucket that's no longer available
remove_samba_share() {
    local bucket_name="$1"

    # Check if the share exists in the Samba config and remove it
    if grep -q "\[$bucket_name\]" /etc/samba/smb.conf; then
        echo "Removing Samba share for $bucket_name..."
        sed -i "/\[$bucket_name\]/,+6d" /etc/samba/smb.conf

        # Unmount the bucket
        fusermount -u /mnt/$bucket_name
        rmdir /mnt/$bucket_name
    fi
}

# Function to dynamically mount buckets and update Samba shares
monitor_buckets() {
    # Create directories for bucket mounts
    mkdir -p /mnt

    # Continuously check for new and removed buckets
    while true; do
        echo "Checking for new and removed S3 buckets..."

        # Get the list of current S3 buckets from rclone
        current_buckets=$(rclone lsd relayer: --no-check-certificate | awk '{print $NF}')

        # Get the list of mounted buckets
        mounted_buckets=$(ls /mnt)

        # Mount new buckets and add to Samba config
        for bucket in $current_buckets; do
            mount_path="/mnt/$bucket"
            if ! mount | grep -q "$mount_path"; then
                echo "Mounting $bucket at $mount_path..."
                mkdir -p $mount_path
                rclone mount relayer:$bucket $mount_path --allow-non-empty --allow-other --uid $(id -u $SHARE_USERNAME) --gid $(id -g root) --no-check-certificate --vfs-cache-mode=${VFS_CACHE_MODE} --vfs-cache-max-size ${VFS_CACHE_MAX_SIZE} --vfs-cache-max-age ${VFS_CACHE_MAX_AGE} --vfs-write-back ${VFS_WRITE_BACK} --vfs-cache-poll-interval ${VFS_CACHE_POLL_INTERVAL} &
                add_samba_share "$bucket"
            fi
        done

        # Remove shares for buckets that no longer exist
        for mounted_bucket in $mounted_buckets; do
            if ! echo "$current_buckets" | grep -q "$mounted_bucket"; then
                echo "Unmounting and removing Samba share for $mounted_bucket..."
                remove_samba_share "$mounted_bucket"
            fi
        done

        # Reload Samba configuration to apply any changes
        echo "Reloading Samba configuration..."
        killall -HUP smbd

        # Wait for 5 seconds before checking again
        sleep 5
    done
}

# Create a system user with no login shell and minimal permissions
echo "Creating system user..."
adduser -D -H -s /sbin/nologin -G root $SHARE_USERNAME
echo "$SHARE_USERNAME:$SHARE_PASSWORD" | chpasswd

# Create a Samba user and set the password
echo "Creating Samba user..."
(echo "$SHARE_PASSWORD"; echo "$SHARE_PASSWORD") | smbpasswd -s -a $SHARE_USERNAME

# Start the Samba daemon
echo "Starting Samba..."
smbd --foreground --no-process-group &

# Start the bucket monitoring process
monitor_buckets