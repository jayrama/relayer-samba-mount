FROM alpine:latest
WORKDIR /app
RUN apk add --no-cache fuse rclone samba bash fuse3 && \
    mkdir /mnt/share

# Copy the setup script
COPY startup.sh /app/startup.sh
RUN chmod +x /app/startup.sh

ENV RCLONE_MOUNT=relayer
ENV MOUNT_PATH=/
ENV VFS_CACHE_MODE=full
ENV VFS_CACHE_MAX_SIZE=10G
ENV VFS_CACHE_MAX_AGE=24h
ENV VFS_CACHE_POLL_INTERVAL=1s
ENV VFS_WRITE_BACK=1s
ENV SHARE_USERNAME=user
ENV SHARE_PASSWORD=pass
ENV MINIO_ACCESS_KEY=123
ENV MINIO_SECRET_KEY=12345678

CMD /app/startup.sh
